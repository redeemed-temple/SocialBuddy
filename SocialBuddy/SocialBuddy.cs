﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Core.UI.Options;
using Newtonsoft.Json;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SocialBuddy.IPCMessages;

namespace SocialBuddy
{


    public class SocialBuddy : AOPluginEntry
    {

        public static Window settingsWindow;
        public static View settingsView;
        public static bool Toggle = false;
        public static string PluginDir;
        public static Settings _settings;
        public static IPCChannel IPCChannel { get; set; }
        public static Config Config { get; private set; }

        internal static Dictionary<uint, string> NameToIdMap = new Dictionary<uint, string>();

        public override void Run(string pluginDir)
        {

            try
            {
                
                _settings = new Settings("SocialBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\SocialBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));
                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.TellMessage, OnIPCTellMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.ListeningMessage, OnIPCListeningMessage);

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;

                SettingsController.RegisterSettingsWindow("SocialBuddy", pluginDir + "\\UI\\SocialBuddySettingWindow.xml", _settings);

                Game.OnUpdate += OnUpdate;
                _settings.AddVariable("Toggle", false);
                _settings["Toggle"] = false;
                Chat.WriteLine("SocialBuddy loaded!");
                Chat.WriteLine("/socialbuddy for settings");

                Network.ChatMessageReceived += Network_ChatMessageReceived;
                
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.ToString());
            }
        }

        public static void OnIPCTellMessage(int sender, IPCMessage msg)
        {
            TellMessage tellMessage = (TellMessage)msg;
            Chat.WriteLine("[" + tellMessage.Character + "] received a tell:", ChatColor.Green);
            Chat.WriteLine(tellMessage.MessageToBroadcast, ChatColor.LightBlue);
        }

        public static void OnIPCListeningMessage(int sender, IPCMessage msg)
        {
            ListeningMessage lMessage = (ListeningMessage)msg;
            Chat.WriteLine("* " + lMessage.Character + " " + lMessage.MessageToBroadcast, ChatColor.Green);
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }




        private void OnStartMessage(int sender, IPCMessage msg)
        {
            _settings["Toggle"] = true;
            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            _settings["Toggle"] = false;
            Stop();
        }

        private void OnUpdate(object sender, float e)
        {
            if (Game.IsZoning) { return; }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                if (channelInput != null)
                {
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    {
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                        Chat.WriteLine("Channel changed to " + channelValue.ToString());
                    }
                }
            }

            if (!_settings["Toggle"].AsBool() && Toggle)
            {
                IPCChannel.Broadcast(new StopMessage());
                Stop();
            }
            if (_settings["Toggle"].AsBool() && !Toggle)
            {
                IPCChannel.Broadcast(new StartMessage());
                Start();
            }

        }


        

        private void Stop()
        {
            Toggle = false;
            Chat.WriteLine("SocialBuddy disabled.");
            IPCChannel.Broadcast(new ListeningMessage()
            {
                Character = DynelManager.LocalPlayer.Name,
                Identity = DynelManager.LocalPlayer.Identity,
                MessageToBroadcast = " no longer will broadcast tell messages!"
            });
        }

        private void Start()
        {
            Toggle = true;
            Chat.WriteLine("SocialBuddy enabled.");
            IPCChannel.Broadcast(new ListeningMessage()
            {
                Character = DynelManager.LocalPlayer.Name,
                Identity = DynelManager.LocalPlayer.Identity,
                MessageToBroadcast = "waiting for tell messages!"
            });
        }

        private void Network_ChatMessageReceived(object s, SmokeLounge.AOtomation.Messaging.Messages.ChatMessageBody chatMessage)
        {
            if (Toggle)
            {
                switch (chatMessage.PacketType)
                {
                    case ChatMessageType.PrivateMessage:
                        var tellmsg = (PrivateMsgMessage)chatMessage;
                        var tellMessage = new IPCMessages.TellMessage();
                        string senderName = "(unknown)";
                        if (NameToIdMap.ContainsKey(tellmsg.Sender))
                        {
                            senderName = NameToIdMap[tellmsg.Sender];
                        }
                        tellMessage.Identity = DynelManager.LocalPlayer.Identity;
                        tellMessage.Character = DynelManager.LocalPlayer.Name;
                        tellMessage.MessageToBroadcast = senderName + " said: ";
                        tellMessage.MessageToBroadcast = tellMessage.MessageToBroadcast + tellmsg.Text;
                        IPCChannel.Broadcast(tellMessage);
                        break;
                    case ChatMessageType.CharacterName:
                        CharacterNameMessage charNameMsg = (CharacterNameMessage)chatMessage;
                        NameToIdMap.Add(charNameMsg.Id, charNameMsg.Name);
                        break;
                    default:
                        break;
                }
            }
        }


        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

    }

}
