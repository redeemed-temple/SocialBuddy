﻿using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using AOSharp.Core;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmokeLounge.AOtomation.Messaging.Serialization;

namespace SocialBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.ListeningMessage)]
    public class ListeningMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.ListeningMessage;

        [AoMember(0)]
        public Identity Identity { get; set; }

        [AoMember(1, SerializeSize = ArraySizeType.Int32)]
        public string Character { get; set; }

        [AoMember(2, SerializeSize = ArraySizeType.Int32)]
        public string MessageToBroadcast { get; set; }
    }
}
