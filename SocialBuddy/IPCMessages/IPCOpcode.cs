﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 501,
        Stop = 502,
        TellMessage = 5001,
        ListeningMessage = 5002
    }
}
